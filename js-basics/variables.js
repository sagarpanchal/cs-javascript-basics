/* eslint-disable no-unused-vars */

/**
 * Variables
 *
 * Named values and can store any type of JavaScript value
 */

{
  // Declaration / Initialization
  var a = 1 // declare a variable - old syntax
  let b = 2 // declare a variable - new syntax
  const c = 3 // declare a constant

  /**
   * Scope
   * `var` has no block scope
   * `let` and `const` are only be accessed inside the scope
   * */
  {
    {
      // `var`
      {
        var varVariable = 'no block scope'
        console.log(varVariable) // accessible inside scope
      }
      console.log(varVariable) // accessible outside scope
    }

    try {
      // let
      {
        let scoppedVariable = 'let variable'
        console.log(scoppedVariable) // accessible inside scope
      }
      // eslint-disable-next-line no-undef
      console.log(scoppedVariable) // not accessible not outside scope (throws ReferenceError)
    } catch (e) {
      console.warn(e)
    }

    try {
      // const
      {
        const scoppedConstant = 'const constant'
        console.log(scoppedConstant) // accessible inside scope
      }
      // eslint-disable-next-line no-undef
      console.log(scoppedConstant) // not accessible outside scope (throws ReferenceError)
    } catch (e) {
      console.warn(e)
    }
  }

  /**
   *  Naming variables
   */
  {
    // After the first letter, you can use numbers, as well as letters, underscores, or dollar signs.
    let camelCase = 'lowercase word, then uppercase'
    let dinner2Go = 'pizza'
    let I_AM_HUNGRY = true
    let A1 = 'A1'

    // Start them with a letter, underscore _, or dollar sign $.
    let _Hello_ = 'what a nice greeting'
    let $_$ = 'money eyes'

    // Can't use any of JavaScript’s reserved keywords. (uncomment to check)
    // let default = 'reserved keyword' // SyntaxError: Unexpected token 'default'
  }

  /**
   * Reassigning variables
   */
  {
    // You can give an existing variable a new value at any point after it’s declared.
    {
      let weather = 'rainy'
      console.log(weather)
      weather = 'sunny'
      console.log(weather)
    }

    // Throws `TypeError` if declared using const
    try {
      const weather = 'rainy'
      console.log(weather)
      // eslint-disable-next-line no-const-assign
      weather = 'sunny' // TypeError: Assignment to constant variable
    } catch (e) {
      console.warn(e)
    }
  }
}
