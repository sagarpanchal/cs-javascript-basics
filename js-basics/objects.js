/**
 * Objects
 *
 * Values that can contain other values.
 * They use keys to name values, which are a lot like variables.
 */

{
  const obj = { name: 'GRA 2032', start: 8, end: 10 }
  console.log(obj)
  console.log({
    type: typeof obj,
    class: obj.constructor.name,
  })

  /**
   * Getting keys
   */
  {
    const course = { name: 'GRA 2032', start: 8, end: 10 }

    // dot notation with the name of the key after a period
    console.log(course.name) // "GRA 2032"

    // bracket notation with the name of the key inside a string inside square brackets [].
    console.log(course['name']) // "GRA 2032"
  }

  /**
   * Settings keys
   */
  {
    const character = { name: 'Donna', hair: 'red' }

    // dot notation with the name of the key after a period
    character.hair = 'blonde'
    console.log(character) // { "name": "Donna", "hair": "blonde" }

    // bracket notation with the name of the key inside a string inside square brackets [].
    character['hair'] = 'red'
    console.log(character) // { "name": "Donna", "hair": "red" }
  }
}
