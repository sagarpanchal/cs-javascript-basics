/* eslint-disable no-constant-condition */

/**
 * Conditionals
 *
 * Conditional statements control behavior in JavaScript and determine whether or not pieces of code can run
 */

{
  /**
   * If...Else Statement Example
   *
   * if statement only runs if the condition enclosed in parentheses () is truthy.
   */
  {
    /** if */
    if (10 > 5) {
      console.log('if block')
    }

    /** if ... else */
    if ('cat' === 'dog') {
      console.log('if block')
    } else {
      console.log('else block')
    } // "else block"

    /** if ... else if */
    if (false) {
      console.log('if block')
    } else if (true) {
      console.log('else if block')
    } else {
      console.log('else block')
    } // "else if block"
  }
}
