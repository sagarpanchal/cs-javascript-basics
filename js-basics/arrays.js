/**
 * Arrays
 *
 * Container-like data structure that can hold other values.
 * The values inside an array are called elements.
 */

{
  const arr = [1, 2, 3, 4, 5]
  console.log(arr)
  console.log({
    type: typeof arr,
    class: arr.constructor.name,
  })

  /**
   * Array elements don’t all have to be the same type of value.
   * Elements can be any kind of JavaScript value — even other arrays.
   */
  {
    const hodgepodge = [100, 'paint', [200, 'brush'], false]
    console.log(hodgepodge)
  }

  /**
   * Accessing Elements
   */
  {
    /**
     * To access one of the elements inside an array,
     * you’ll need to use the brackets and a number like this: myArray[3].
     * JavaScript arrays begin at 0, so the first element will always be inside [0].
     */
    const sisters = ['Tia', 'Tamera']
    console.log(sisters[0]) // "Tia"

    /**
     * To get the last element, you can use brackets and `1` less than the array’s length property
     */
    let actors = ['Felicia', 'Nathan', 'Neil']
    console.log(actors[actors.length - 1]) // "Neil"

    /**
     * This also works for setting an element’s value.
     */
    let colors = ['red', 'yelo', 'blue']
    console.log(colors) // ["red", "yelo", "blue"]
    colors[1] = 'yellow'
    console.log(colors) // ["red", "yelllow", "blue"]
  }

  /**
   * Properties and methods
   */
  {
    /** length - number of elements */
    console.log(['a', 'b', 'c', 1, 2, 3].length) // 6

    /** concat - returns a new array that combines the values of two arrays */
    console.log(['tortilla chips'].concat(['salsa', 'queso', 'guacamole'])) // ["tortilla chips", "salsa", "queso", "guacamole"]

    /** pop - emoves the last element in the array and returns that element’s value */
    console.log(['Jupiter', 'Saturn', 'Uranus', 'Neptune', 'Pluto'].pop()) // "Pluto"

    /** push - adds an element to the array and returns the array’s length */
    console.log(['John', 'Kate'].push(8)) // 3

    /** reverse - returns a copy of the array in opposite order */
    console.log(['a', 'b', 'c'].reverse()) // ["c", "b", "a"]
  }
}
