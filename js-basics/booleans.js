/**
 * Boolean
 *
 * Values that can be only one of two things: true or false.
 * Anything “on” or “off,” “yes” or “no,” or temporary is a usually good fit for a boolean.
 * It’s useful to store booleans in variables to keep track of their values and change them over time.
 */

{
  let bool = true
  console.log(bool)
  bool = false
  console.log(bool)
  console.log({
    type: typeof bool,
    class: bool.constructor.name,
  })
}
