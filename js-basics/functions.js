/* eslint-disable no-unused-vars */

/**
 * Functions
 *
 * Blocks of code that can be named and reused.
 */

{
  /** Declaration */
  // eslint-disable-next-line no-inner-declarations
  function addTwoNumbers1(x, y) {
    return x + y
  }

  /** calling */
  const output = addTwoNumbers1(1, 2)
  console.log(output)

  /**
   * new Function()
   */
  {
    const addTwoNumbers = new Function('a', 'b', 'return a + b')
    console.log(addTwoNumbers(2, 3))
  }

  /**
   * Arrow functions
   *
   * Do not have `this`, `arguments`, `super`,
   * Can’t be called with `new`
   */
  {
    const addTwoNumbers2 = (a, b) => {
      return a + b
    }
    // shorthand for return
    const addTwoNumbers1 = (a, b) => a + b
  }
}
