/**
 * Strings
 *
 * Values made up of text and can contain letters, numbers, symbols, punctuation, and even emoji.
 */

{
  const str = 'This is a string. 👏'
  console.log(str)
  console.log({
    type: typeof str,
    class: str.constructor.name,
  })

  /** Strings are wrapped in single quotes, double quotes and backticks */
  console.log('This is the 1st string. 💁')
  // prettier-ignore
  console.log("This is the 2nd string. 💁")

  console.log(`This is the 3rd string. 💁`)

  /**
   * Enclosing quotation marks
   *
   * Use single quote inside double and double quotes inside single quoteed string
   * Alternatively, you can use a backslash \ to escape the quotation marks.
   */
  {
    // prettier-ignore
    console.log("It's six o'clock.")

    console.log('Remember to say "please" and "thank you."')
    // prettier-ignore
    console.log("It\"s six o\"clock.")
    // prettier-ignore
    console.log('Remember to say \'please\' and \'thank you.\'')
  }

  /**
   * Properties and methods
   */
  {
    /** length - total number os letter in a string */
    console.log('caterpillar'.length)

    /** toLowerCase - convert letters to lowercase */
    console.log('THE KIDS'.toLowerCase())

    /** toUpperCase - convert letters to uppercase */
    console.log('the kids'.toUpperCase())

    /** trim - remove spaces from start and end */
    console.log('   but keeps spaces in-between   '.trim())
  }
}
