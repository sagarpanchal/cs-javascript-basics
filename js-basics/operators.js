/**
 * Operators
 *
 * Symbols between values that allow different operations
 * like addition, subtraction, multiplication, and more.
 */

{
  /**
   * Arithmetic
   */
  {
    /** The + operator adds two numbers */
    console.log(1 + 2)

    /** The - operator subtracts one number from another */
    console.log(50 - 15)

    /** The * operator multiplies two numbers */
    console.log(3 * 12)

    /** The / operator divides one number by another */
    console.log(12 / 4)

    /** JavaScript expressions follow an order of operations (PEMDAS )  */
    console.log(1 + 100 * 5) // 501
  }

  /**
   * Grouping
   */
  {
    /**
     * () operator groups other values and operations.
     * Code located between parentheses evaluates first as JavaScript solves each operation moving from left to right.
     * Adding the grouping operator to the previous example causes 1 + 100 to evaluate first.
     */
    console.log((1 + 100) * 5) // 505
  }

  /**
   * Concatenation
   */
  {
    /** The + operator can also concatenate strings, which is another way of saying it can add them together */
    console.log('news' + 'paper') // "newspaper"
  }

  /**
   * Assignment
   */
  {
    /** The = operator assigns values. It’s used for setting the value of variables */
    const dinner = 'sushi'
    console.log(dinner)
  }
}
