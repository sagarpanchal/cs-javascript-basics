/**
 * Numbers
 *
 * Values that can be used in mathematical operations.
 * You don’t need any special syntax for numbers — just write them straight into JavaScript.
 */

{
  const n = 12345
  console.log(n)
  console.log({
    type: typeof n,
    class: n.constructor.name,
  })

  /**
   * Decimals and fractions
   */
  {
    /**
     * JavaScript doesn’t distinguish between whole numbers and decimals,
     * so you can use them together without having to convert from one to the other.
     */
    console.log(10 + 3.14159)

    /**
     * Fractions don’t exist in JavaScript,
     * but you can rewrite them as division problems using the division operator /.
     * Note that the resulting number is always converted to decimals — just like with a calculator.
     */
    console.log(1 / 3)

    /**
     * Improper fractions use the division operator in the same way.
     */
    console.log(11 / 10)

    /**
     * To use mixed numbers, you need to add the whole number and fraction.
     */
    console.log(1 + 4 / 3)
  }

  /**
   * Negative numbers
   */
  {
    /**
     * You can make a number negative by placing the - operator in front.
     */
    console.log(-3)

    /**
     * You can also get a negative number by subtracting a number from a smaller number.
     */
    console.log(5 - 7)
  }
}
